<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>OC Landing</title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=1260">
    <link rel="stylesheet" href="/css/index.min.css">
    <link
        href='https://fonts.googleapis.com/css?family=Roboto:300italic,400italic,500,500italic,700,700italic&subset=latin,cyrillic'
        rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300&subset=latin,cyrillic' rel='stylesheet'
          type='text/css'>

    <link rel="apple-touch-icon" sizes="57x57" href="/img/fav/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/fav/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/fav/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/fav/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/fav/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/fav/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/fav/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/fav/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/fav/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/img/fav/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/img/fav/favicon-194x194.png" sizes="194x194">
    <link rel="icon" type="image/png" href="/img/fav/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/img/fav/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/img/fav/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/img/fav/manifest.json">
    <link rel="mask-icon" href="/img/fav/safari-pinned-tab.svg" color="#e32313">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <style>
        .Nav{filter:url(#drop-shadow)}
        @supports (-webkit-filter:drop-shadow(0 5px 10px rgba(0,0,0,.3))){.Nav{-webkit-filter:drop-shadow(0 5px 10px rgba(0,0,0,.3))}}
    </style>
</head>

<body>
<?php require_once('img/sprite.svg'); ?>
<?php require_once('img/shadow.svg'); ?>
<?php require(__DIR__ . '/templates/01-nav.php'); ?>
<main>
    <?php

    require(__DIR__ . '/templates/02-about-us.php');
    require(__DIR__ . '/templates/03-our-goal.php');
    require(__DIR__ . '/templates/04-forecast.php');
    require(__DIR__ . '/templates/05-what-we-do.php');
    require(__DIR__ . '/templates/06-leave-request.php');
    require(__DIR__ . '/templates/07-roadmap.php');
    require(__DIR__ . '/templates/08-cases.php');
    require(__DIR__ . '/templates/09-our-team.php');
    require(__DIR__ . '/templates/10-client-reviews.php');
    require(__DIR__ . '/templates/11-promotion-cost.php');
    require(__DIR__ . '/templates/12-seo-team.php');
    require(__DIR__ . '/templates/13-leave-request-2.php');
    require(__DIR__ . '/templates/14-faq.php');
    require(__DIR__ . '/templates/15-footer.php');
    ?>
</main>

<script src="/index.min.js"></script>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
<script>

    ymaps.ready(init);

    function init () {
        var brusov = [55.7375, 37.6068];
        var myMap = new ymaps.Map("map_moscow", {
                center: brusov,
                zoom: 16
            }, {
                searchControlProvider: 'yandex#search'
            }),

            myGeoObject = new ymaps.GeoObject({
                geometry: {
                    type: "Point",
                    coordinates: brusov
                },
                properties: {
                    iconContent: 'Корабль «Брюсов»',
                }
            }, {
                preset: 'islands#redStretchyIcon'
            });

        myMap.geoObjects.add(myGeoObject);
    }
</script>
</body>
</html>