//  Menu
$('.Nav-Hamburger').click(function () {
    $('.Nav-Menu').css('left', '0px');
    return false;
});

$('.Nav-Menu .Nav-Close, .Nav-Menu a, main').click(function () {
    $('.Nav-Menu').css('left', '-340px');
});

// Map Tabs
$('.Footer-Tabs .Switch input').click(function () {
    if ($(this).is(':checked')) {
        $(this).parents('.Footer-Tabs').find('.Tab').siblings().removeClass('Active');
        $(this).parents('.Footer-Tabs').find('.Tab:last-of-type').addClass('Active');
        $('.Maps .Map:last-of-type').addClass('Active').siblings().removeClass('Active');
    } else {
        $(this).parents('.Footer-Tabs').find('.Tab').siblings().removeClass('Active');
        $(this).parents('.Footer-Tabs').find('.Tab:first-of-type').addClass('Active');
        $('.Maps .Map:first-of-type').addClass('Active').siblings().removeClass('Active'); 
    }
});
 
$('.Footer-Tabs .Tab').click(function () {

    $(this).siblings().removeClass('Active');
    $(this).addClass('Active');

    if ($(this).is(':last-of-type')) {
        $('.Footer-Tabs .Switch input').prop("checked", true);
        $('.Maps .Map:last-of-type').addClass('Active').siblings().removeClass('Active');
    } else {
        $('.Footer-Tabs .Switch input').prop("checked", false);
        $('.Maps .Map:first-of-type').addClass('Active').siblings().removeClass('Active');
    }
});


// FAQ Items
$('.Faq-Item .Question').click(function () {
    $(this).next().slideDown(300).parent().addClass('Active');
    $(this).parent().siblings().find('.Answer').slideUp(300);
    $(this).parent().siblings().removeClass('Active');
});

// Team Popups
$('.Team-Graph .Person').mouseover(function (Event) {
    $(this).find('.Pop-Up').fadeIn(100);
    return false;
});

$('.Team-Graph .Person').mouseleave(function (Event) {
    $(this).find('.Pop-Up').fadeOut(50);
    return false;
});


// Cases Tabs
$('.Cases-Tabs .Tab-Captions li').click(function () {
    $(this).addClass('Active').siblings().removeClass('Active');

    $($('.Cases-Tabs .Tab-Content-Item').get($(this).index()))
        .addClass('Active').siblings().removeClass('Active');
});

// Ended Slider-Tabs
$('.Leave-Request-Wide .Steps .Slider-Arrow.Next').click(function () {

    if ($('.Leave-Request-Wide .Slider-Content li.Active').next().length) {
        $('.Leave-Request-Wide .Steps-Graph li.Active').next().addClass('Active');
        $('.Leave-Request-Wide .Slider-Content li.Active').removeClass('Active').next().addClass('Active');
        $('.Leave-Request-Wide .Steps .Slider-Arrow.Prev').fadeIn(200);
    } else {
        $('.Leave-Request-Wide .Steps .Slider-Arrow.Next').fadeOut(200);
    }


    $('.Leave-Request-Wide .Steps-Graph .Active').index();
    return false;
});

// Click to prev
$('.Leave-Request-Wide .Steps .Slider-Arrow.Prev').click(function () {
    if ($('.Leave-Request-Wide .Slider-Content li.Active').prev().length) {
        $('.Leave-Request-Wide .Steps-Graph li.Active:last').removeClass('Active');
        $('.Leave-Request-Wide .Slider-Content li.Active').removeClass('Active').prev().addClass('Active');
        $('.Leave-Request-Wide .Steps .Slider-Arrow.Next').fadeIn(200);
    } else {
        $('.Leave-Request-Wide .Steps .Slider-Arrow.Prev').fadeOut(200);
    }

    return false;
});

// Click to tabs
$('.Leave-Request-Wide .Steps-Graph li').click(function () {
    $(this).addClass('Active');
    $(this).prevAll().addClass('Active');
    $(this).nextAll().removeClass('Active');

    $($('.Leave-Request-Wide .Slider-Content li').get($(this).index())).addClass('Active');
    $($('.Leave-Request-Wide .Slider-Content li').get($(this).index())).siblings().removeClass('Active');

    if ($(this).index() == 0) {
        $('.Leave-Request-Wide .Steps .Slider-Arrow.Prev').fadeOut(200);
    } else {
        $('.Leave-Request-Wide .Steps .Slider-Arrow.Prev').fadeIn(200);
    }

    if ($(this).index() == 5) {
        $('.Leave-Request-Wide .Steps .Slider-Arrow.Next').fadeOut(200);
    } else {
        $('.Leave-Request-Wide .Steps .Slider-Arrow.Next').fadeIn(200);
    }
});

// Order Heightlight
$('.Order-Hightlight').click(function () {
    $('.Leave-Request').addClass('Heightlighted');

    setTimeout(function () {
        $('.Leave-Request').removeClass('Heightlighted');
    }, 300);
    return false;
});

// Mask
$('input[name="phone"]').mask('+7 (999) 999-99-99');

