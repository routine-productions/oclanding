<section class="Nav">
    <a class="Nav-Hamburger" href="#">
        <span class="Hamburger"></span>
        <p>Меню</p>
    </a>

    <img class="Nav-Logo JS-Scroll-Top-Button" src="/img/logo.svg" width="175" alt="">

    <div class="Nav-Contacts">
        <div class="Nav-City">
            <span>Москва</span>
            <a href="tel:+74959301525">+7 (495) 930-15-25</a>
        </div>
        <div class="Nav-City">
            <span>Санкт-Петербург</span>
            <a href="tel:+78124331280">+7 (812) 433-15-80</a>
        </div>
    </div>

    <div class="Nav-Menu JS-Page-Navigation" data-scroll-shift="88">
        <ul>
            <li><a href="#about">О нас</a></li>
            <li><a href="#goal">Наши цели</a></li>
            <li><a href="#forecast">Прогнозы</a></li>
            <li><a href="#what-we-do">Что мы делаем</a></li>
            <li><a href="#roadmap">Доходы</a></li>
            <li><a href="#cases">Кейсы</a></li>
            <li><a href="#team">Наша команды</a></li>
            <li><a href="#reviews">Отзывы клиентов</a></li>
            <li><a href="#cost">Стоимость продвижения</a></li>
            <li><a href="#seo">Команда SEO</a></li>
            <li><a href="#faq">Вопросы и ответы</a></li>
            <li><a href="#contacts">Контакты</a></li>
            <li class="Nav-Order Btn Primary"><a href="#leave-request">Оставить заявку</a></li>
        </ul>
        <div class="Nav-Close">×</div>
    </div>
</section>

