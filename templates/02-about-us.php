<section class="About-Us" id="about">
    <header>
        <h2>Продвигаем бизнес, строим продажи</h2>
        <p>Приводим клиентов из поисковых систем, увеличиваем прибыль</p>
    </header>

    <div class="First-Screen-Banner" >
        <a href="#leave-request" class="Btn JS-Scroll-Button"><span>Оставить заявку</span></a>
    </div>

    <a href="#leave-request" class="Btn Primary JS-Scroll-Button">Оставить заявку на КП</a>
</section>