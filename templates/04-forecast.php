<section class="Forecast" id="forecast">
    <header class="Section-Heading">
        <h2>Прогнозируем прибыль до начала работ</h2>
    </header>

    <div class="Slider Forecast-Carousel JS-Carousel">
        <ul class="Slider-Content JS-Carousel-List">
            <li class="JS-Carousel-Item">
                <img src="/img/slide-01.png" srcset="/img/slide-01@2x.png 2x" alt="">
            </li>
            <li class="JS-Carousel-Item">
                <img src="/img/slide-02.png" srcset="/img/slide-02@2x.png 2x" alt="">
            </li>
            <li class="JS-Carousel-Item">
                <img src="/img/slide-03.png" srcset="/img/slide-03@2x.png 2x" alt="">
            </li>
        </ul>
    </div>
</section>