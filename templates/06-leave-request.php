<!-- SECTION: Оставить заявку -->
<section class="Leave-Request" id="leave-request">
    <header>
        <h2>Оставить заявку
            <p>
                <svg><use xlink:href="#bubble"></use></svg>
                В рабочее время ответим через 15 минут
            </p>
        </h2>
    </header>

    <form class='JS-Form'
          data-form-email='sales@5oclick.ru'
          data-form-subject='Order form website'
          data-form-url='/js/form.php'
          data-form-method='POST'>

        <div class="Row">
            <input type="text" placeholder="Ваше имя" name="name" data-input-title='Имя' class="JS-Form-Require">
        </div>

        <div class="Row Double">
            <input type="tel" placeholder="Телефон" name="phone" data-input-title='Телефон' class="JS-Form-Require">
            <span>или</span>
            <input type="text" placeholder="Эл. почту" name="email" data-input-title='Email'
                   class="JS-Form-Require JS-Form-Email">
        </div>
        <div class="Row">
            <input type="text" placeholder="Адрес сайта, который нужно продвигать" name="website"
                   data-input-title='Website'>
        </div>
        <div class="Row">
            <textarea placeholder="Краткое описание задачи" name="message"
                      data-input-title='Описание задачи:'></textarea>
        </div>

        <button class="Btn Secondary JS-Form-Button">Отправить »</button>
        <div class="JS-Form-Result"></div>
    </form>
</section>