<section class="Roadmap" id="roadmap">
    <header class="Section-Heading">
        <h2>Не обещаем быстрых результатов</h2>
    </header>
    <p class="Intro">
        Короткий текст о том, что комплексное продвижение требует долгосрочных усилий.
    </p>
    <div class="Months">

        <ul class="Graph-Year">
            <li>
                <span>Подробный аудит.<br>Сбор и кластеризация семантического ядра
            </span>
            </li>

            <li class="Bracket Push">
                <span>Внутренняя<br>
                оптимизация сайта</span>
            </li>

            <li class="Push">
                <span>Подготовка и&nbsp;усиление&nbsp;контента</span>
            </li>

            <li class="Push">
                <span>Аудит
                переработки</span>
            </li>

            <li  class="Bracket"></li>

            <li>
                <span>Внедрение<br>
                рекомендаций</span>
            </li>

            <li></li>

            <li class="Bracket Wide">
                <span>Рост продаж
                в среднем — на 30%</span>
            </li>

            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>
    </div>

    <div class="Zigzag">
        <?php require_once __DIR__ . '/../img/zigzag-2.svg' ?>
    </div>
    <div class="Slider JS-Carousel Facts-Carousel">
        <ul class="Slider-Content JS-Carousel-List">
            <li class="JS-Carousel-Item">
                <article class="Food">
                    <div class="Roadmap-Button">
                        <button>Доход от SEO</button>
                    </div>
                    <div class="Roadmap-Content">
                        <h3>Сфера питания</h3>
                        <h4>Чешский пивной ресторан в Петербурге</h4>
                        <div class="Schema Row-5">
                            <div class="Column">
                                <div class="Rectangle" data-height="2"></div>
                                <b>2 100 ₽</b>
                                <span>месяц 1</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="7"></div>
                                <b>7 890 ₽</b>
                                <span>месяц 3</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="17"></div>
                                <b>20 940 ₽</b>
                                <span>месяц 6</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="25"></div>
                                <b>28 830 ₽</b>
                                <span>месяц 9</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="34"></div>
                                <b>37 830 ₽</b>
                                <span>месяц 12</span>
                            </div>
                        </div>

                        <p>За год работы доход ресторана<br> благодаря СЕО вырос в 18 раз: c 2 100 до 37 830 ₽</p>
                    </div>
                </article>
            </li>

            <li class="JS-Carousel-Item">
                <article class="Food">
                    <div class="Roadmap-Button">
                        <button>Цена клиента</button>
                    </div>
                    <div class="Roadmap-Content">
                        <h3>Сфера питания</h3>
                        <h4>Чешский пивной ресторан в Петербурге</h4>
                        <div class="Schema Row-5">
                            <div class="Column">
                                <div class="Rectangle" data-height="30"></div>
                                <b>15 000 ₽</b>
                                <span>месяц 1</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="9"></div>
                                <b>3 992 ₽</b>
                                <span>месяц 3</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="3"></div>
                                <b>1 504 ₽</b>
                                <span>месяц 6</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="2"></div>
                                <b>1 092 ₽</b>
                                <span>месяц 9</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="1"></div>
                                <b>842 ₽</b>
                                <span>месяц 12</span>
                            </div>
                        </div>
                        <p>За год работы цена за привлечённого клиента снизилась в 18 раз:<br>15 тысяч до 842₽</p>
                    </div>
                </article>
            </li>

            <li class="JS-Carousel-Item">
                <article class="Food">
                    <div class="Roadmap-Button">
                        <button>ROI от СЕО</button>
                    </div>
                    <div class="Roadmap-Content">
                        <h3>Сфера питания</h3>
                        <h4>Чешский пивной ресторан в Петербурге</h4>
                        <div class="Schema Row-5 Shifted">
                            <div class="Column Minus">
                                <div class="Rectangle" data-height="26"></div>
                                <b>-15 900 ₽</b>
                                <span>месяц 1</span>
                            </div>
                            <div class="Column Minus">
                                <div class="Rectangle" data-height="16"></div>
                                <b>-10 110 ₽</b>
                                <span>месяц 3</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="5"></div>
                                <b>2 940 ₽</b>
                                <span>месяц 6</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="18"></div>
                                <b>10 830 ₽</b>
                                <span>месяц 9</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="33"></div>
                                <b>19 830 ₽</b>
                                <span>месяц 12</span>
                            </div>
                        </div>
                        <p>За год работы чистая прибыль<br>ресторана благодаря SEO выросла<br> на 35 280 ₽ &ndash; до 19
                            380 ₽</p>
                    </div>
                </article>
            </li>

            <li class="JS-Carousel-Item">
                <article class="Finance">
                    <div class="Roadmap-Button">
                        <button>Прирост<br>потенциальных<br>клиентов</button>
                    </div>
                    <div class="Roadmap-Content">
                        <h3>Финансы</h3>
                        <h4>Сложный финансовый продукт в узкой<br>нише — платёжный агрегатор</h4>
                        <div class="Schema Row-6">
                            <div class="Column">
                                <div class="Rectangle" data-height="3"></div>
                                <b>17</b>
                                <span>месяц 1</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="10"></div>
                                <b>50</b>
                                <span>месяц 5</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="12"></div>
                                <b>64</b>
                                <span>месяц 10</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="16"></div>
                                <b>83</b>
                                <span>месяц 14</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="22"></div>
                                <b>106</b>
                                <span>месяц 18</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="21"></div>
                                <b>105</b>
                                <span>месяц 22</span>
                            </div>
                        </div>
                        <p>За 22 месяца работы число потенциальных клиентов выросло<br>в 6 раз: с 17 человек в месяц до
                            105</p>
                    </div>
                </article>
            </li>
            <li class="JS-Carousel-Item">
                <article class="Finance">
                    <div class="Roadmap-Button">
                        <button>Стоимость<br>потенциального<br>клиента</button>
                    </div>
                    <div class="Roadmap-Content">
                        <h3>Финансы</h3>
                        <h4>Сложный финансовый продукт в узкой<br>нише — платёжный агрегатор</h4>
                        <div class="Schema Row-6 Shifted-Huge">
                            <div class="Column">
                                <div class="Rectangle" data-height="40"></div>
                                <b>2 117 ₽</b>
                                <span>месяц 1</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="15"></div>
                                <b>720 ₽</b>
                                <span>месяц 5</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="13"></div>
                                <b>734 ₽</b>
                                <span>месяц 10</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="11"></div>
                                <b>553 ₽</b>
                                <span>месяц 14</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="9"></div>
                                <b>480 ₽</b>
                                <span>месяц 18</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="7"></div>
                                <b>343 ₽</b>
                                <span>месяц 22</span>
                            </div>
                        </div>
                        <p class="Shifted">За 22 месяца работы мы снизили стоимость<br>потенциального клиента с 2 117 ₽ до 343 ₽</p>
                    </div>

                </article>
            </li>

            <li class="JS-Carousel-Item">
                <article class="Cloud">
                    <div class="Roadmap-Button">
                        <button>Прирост<br>потенциальных<br>клиентов</button>
                    </div>
                    <div class="Roadmap-Content">
                        <h3>Облачные технологии</h3>
                        <h4>Облачный IT-аутсорсинг для малого бизнеса</h4>
                        <div class="Schema Row-6">
                            <div class="Column">
                                <div class="Rectangle" data-height="7"></div>
                                <b>30</b>
                                <span>месяц 1</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="9"></div>
                                <b>45</b>
                                <span>месяц 3</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="14"></div>
                                <b>55</b>
                                <span>месяц 5</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="18"></div>
                                <b>73</b>
                                <span>месяц 8</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="24"></div>
                                <b>105</b>
                                <span>месяц 10</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="28"></div>
                                <b>127</b>
                                <span>месяц 15</span>
                            </div>
                        </div>
                        <p>За 15 месяцев работы число потенциальных клиентов выросло вчетверо: с 30 до 127</p>
                    </div>
                </article>
            </li>

            <li class="JS-Carousel-Item">
                <article class="Cloud">
                    <div class="Roadmap-Button">
                        <button>Стоимость<br>потенциального<br>клиента</button>
                    </div>
                    <div class="Roadmap-Content">
                        <h3>Облачные технологии </h3>
                        <h4>Облачный IT-аутсорсинг для малого бизнеса</h4>
                        <div class="Schema Row-6">
                            <div class="Column">
                                <div class="Rectangle" data-height="37"></div>
                                <b>1 167 ₽</b>
                                <span>месяц 1</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="24"></div>
                                <b>1 111 ₽</b>
                                <span>месяц 3</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="20"></div>
                                <b>909 ₽</b>
                                <span>месяц 5</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="17"></div>
                                <b>685 ₽</b>
                                <span>месяц 8</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="13"></div>
                                <b>446 ₽</b>
                                <span>месяц 10</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="9"></div>
                                <b>394 ₽</b>
                                <span>месяц 15</span>
                            </div>
                        </div>
                        <p>За 15 месяцев работы число потенциальных клиентов выросло<br>вчетверо: с 30 до 127</p>
                    </div>
                </article>
            </li>

            <li class="JS-Carousel-Item">
                <article class="Cloud">
                    <div class="Roadmap-Button">
                        <button>Прирост<br>реальных<br>клиентов</button>
                    </div>
                    <div class="Roadmap-Content">
                        <h3>Облачные технологии </h3>
                        <h4>Облачный IT-аутсорсинг для малого бизнеса</h4>
                        <div class="Schema Row-6">
                            <div class="Column">
                                <div class="Rectangle" data-height="6"></div>
                                <b>11</b>
                                <span>месяц 1</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="18"></div>
                                <b>30</b>
                                <span>месяц 3</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="12"></div>
                                <b>21</b>
                                <span>месяц 5</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="18"></div>
                                <b>30</b>
                                <span>месяц 8</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="30"></div>
                                <b>52</b>
                                <span>месяц 10</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="28"></div>
                                <b>49</b>
                                <span>месяц 15</span>
                            </div>
                        </div>
                        <p>За 15 месяцев работы число новых заявок на подключение<br>выросло в 4,5 раза: с 11 до 49 в
                            месяц</p>
                    </div>
                </article>
            </li>

            <li class="JS-Carousel-Item">
                <article class="Services">
                    <div class="Roadmap-Button">
                        <button>Прирост<br>новых<br>пользователей</button>
                    </div>
                    <div class="Roadmap-Content">
                        <h3>Сервисы-агрегаторы</h3>
                        <h4>Крупный онлайн-сервис с базой поставщиков автоуслуг</h4>
                        <div class="Schema Row-5">
                            <div class="Column">
                                <div class="Rectangle" data-height="6"></div>
                                <b>449</b>
                                <span>месяц 1</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="7"></div>
                                <b>507</b>
                                <span>месяц 3</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="14"></div>
                                <b>920</b>
                                <span>месяц 6</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="16"></div>
                                <b>1054</b>
                                <span>месяц 9</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="20"></div>
                                <b>1305</b>
                                <span>месяц 11</span>
                            </div>
                        </div>
                        <p>За 11 месяцев работы число новых пользователей увеличилось<br>почти втрое: с 449 до 1 305
                            человек в месяц</p>
                    </div>
                </article>
            </li>

            <li class="JS-Carousel-Item">
                <article class="Services">
                    <div class="Roadmap-Button">
                        <button>Стоимость<br>нового<br>пользователя</button>
                    </div>
                    <div class="Roadmap-Content">
                        <h3>Сервисы-агрегаторы</h3>
                        <h4>Крупный онлайн-сервис с базой поставщиков автоуслуг</h4>
                        <div class="Schema Row-5">
                            <div class="Column">
                                <div class="Rectangle" data-height="26"></div>
                                <b>100 ₽</b>
                                <span>месяц 1</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="23"></div>
                                <b>88 ₽</b>
                                <span>месяц 3</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="14"></div>
                                <b>49 ₽</b>
                                <span>месяц 6</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="10"></div>
                                <b>42 ₽</b>
                                <span>месяц 9</span>
                            </div>
                            <div class="Column">
                                <div class="Rectangle" data-height="8"></div>
                                <b>34 ₽</b>
                                <span>месяц 11</span>
                            </div>
                        </div>
                        <p>За 11 месяцев работы цена за нового пользователя со 100 рублей до 34</p>
                    </div>
                </article>
            </li>
        </ul>
    </div>


    <section class="Facts-and-Numbers">
        <h4>Цифры и факты</h4>
        <ul>
            <li>
                <strong><?php require_once __DIR__ . '/../img/numbers/11.svg' ?></strong>
                лет<br>
                продвигаем сайты
            </li>
            <li>
                <strong><?php require_once __DIR__ . '/../img/numbers/150.svg' ?></strong><br>
                клиентов
            </li>
            <li>
                <strong><?php require_once __DIR__ . '/../img/numbers/370000.svg' ?></strong><br>
                переходов в месяц<br>у наших клиентов
            </li>
            <li>
                <strong><?php require_once __DIR__ . '/../img/numbers/95-persent.svg' ?></strong><br>
                клиентов приходят<br>по рекомендации
            </li>
            <li>
                <strong><?php require_once __DIR__ . '/../img/numbers/15.svg' ?></strong>
                <span>месяцев<br>
                средний срок<br>
                контракта</span>
            </li>
        </ul>
    </section>

</section>