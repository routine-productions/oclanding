<section class="Cases" id="cases">
    <header class="Section-Heading">
        <h2>Кейсы</h2>
    </header>
    <p class="Intro">
        Несколько общих цифр: за 11 лет через нас прошло 127 клиентов.
        Предпочитаем работать с отраслями:
    </p>

    <div class="Cases-Tabs">
        <ul class="Tab-Captions">
            <li class="Tab-Captions-Item Active">
                <h6>Интернет&mdash;магазин израильской косметики</h6>
                <ul>
                    <li>Доход 1 960 000 (за год)</li>
                    <li>ROMI 145%</li>
                </ul>
            </li>
            <li class="Tab-Captions-Item">
                <h6>Сеть пивных ресторанов</h6>
                <ul>
                    <li>Доход 1 960 000 (за год)</li>
                    <li>ROMI 145%</li>
                </ul>
            </li>
        </ul>
        <ul class="Tab-Content">

            <li class="Tab-Content-Item Active">

                <div class="Slider Cases-Carousel JS-Carousel">

                    <div class="Slider-Content JS-Carousel-List">
                        <div class="Item JS-Carousel-Item Cosmetics">
                            <h5>Задача</h5>
                            <div class="Cases-Item-Center">
                                <p>
                                    <b>На старте:</b> почти нулевой органический трафик<br>
                                    <b>Задача:</b> рост трафика и заказов, окупаемость инвестиций
                                </p>
                            </div>
                        </div>

                        <div class="Item JS-Carousel-Item Cosmetics">
                            <h5>Стратегия</h5>
                            <div class="Cases-Item-Inner">
                                <ul class="Checklist">
                                    <li>
                                        <strong>Работа с ссылками</strong>
                                        Безопасное планомерное развитие ссылочной базы. Только белые методы — избегаем
                                        фильтров. Делаем упор на естественные (безанкорные) ссылки. Работаем с
                                        тематическими
                                        вечными ссылками (до 20 в месяц).
                                    </li>
                                    <li>
                                        <strong>Работа с контентом</strong>
                                        Карточки товаров, отзывы клиентов, свой канал на Ютубе. Видео дополнительно
                                        размещали на сайте. Это&nbsp;помогло задержать посетителей на&nbsp;сайте и
                                        улучшить
                                        пользовательские факторы
                                    </li>
                                    <li>
                                        <strong>Внутренняя<br>оптимизация</strong>
                                        Мы провели аудит, проверили 200&nbsp;пунктов на сайте, составили подробные&nbsp;рекомендации.
                                    </li>
                                    <li>
                                        <strong>Возвращение посетителей&nbsp;на&nbsp;сайт</strong>
                                        Работали с брошенными корзинами через специализированный сервис.
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="Item JS-Carousel-Item Cosmetics">
                            <h5>Результаты</h5>
                            <div class="Cases-Item-Inner">
                                <div class="Legend">
                                    <strong>Мы настроили метрики для&nbsp;отслеживания&nbsp;покупок</strong>
                                    <ul>
                                        <li>Годовой бюджет на SEO: 240 000 ₽</li>
                                        <li>Доход: 1 960 000 ₽ (за год работы)</li>
                                        <li>Прибыль (при марже в 30%): 588 000 ₽</li>
                                        <li>ROMI: 145%</li>
                                    </ul>
                                </div>
                                <img src="/img/graph-2.png" alt="" class="Cases-Graph">
                            </div>
                        </div>
                        <div class="Item JS-Carousel-Item Cosmetics">
                            <h5>Продолжение результатов</h5>
                            <div class="Cases-Item-Inner">
                                <div class="Legend">
                                    <p>
                                        Благодаря SvEO мы увеличили органический трафик
                                        с 800 посетителей
                                        в месяц до 3 400
                                        (в сезон — до&nbsp;5&nbsp;000)
                                    </p>
                                </div>

                                <img src="/img/graph.png" alt="" class="Graph">

                                <p class="About">
                                    Рост дохода —
                                    <strong>с&nbsp;0&nbsp;до&nbsp;150&nbsp;000&nbsp;₽</strong>
                                    в&nbsp;месяц
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </li>


            <li class="Tab-Content-Item">

                <div class="Slider Cases-Carousel JS-Carousel">

                    <div class="Slider-Content JS-Carousel-List">
                        <div class="Item JS-Carousel-Item Beer">
                            <header>
                                <h5>Beer Family</h5>
                                <p>сеть пивных ресторанов с различными специализациями:</p>
                            </header>

                            <div about="Cases-Item-Inner">
                                <div class="Caption">
                                    у каждого &mdash; свой домен и сайт
                                </div>

                                <ul class="Cones">
                                    <li>Kriek — бельгийская брассерия</li>
                                    <li>Jagerhaus — немецкие рестораны</li>
                                    <li>Чешские Пивовары — чешский ресторан</li>
                                </ul>
                            </div>
                        </div>

                        <div class="Item JS-Carousel-Item Beer">
                            <header>
                                <h5>Задача</h5>
                                <small>Рост числа бронирований столиков и посещений,<br> окупаемость кампаний</small>
                            </header>
                            <div class="Cases-Item-Inner">
                                <p class="fsz-22 fw-300">Подзадачи</p>
                                <div class="Bracket-2">
                                    <span>Продвижение конкретных ресторанов, а не сети</span>
                                    <span>Продвижение информационного портала о пиве</span>
                                    <span>Продвижение с упором на услуги: веранда, детская одежда, футбольные матчи</span>
                                </div>
                            </div>
                        </div>

                        <div class="Item JS-Carousel-Item Beer">
                            <header>
                                <h5>Стратегия</h5>
                                <small>Основа стратегии — правильный подбор ключевых слов</small>
                            </header>


                            <div class="Cases-Item-Inner">


                                <ul class="Checklist">
                                    <li>
                                        <strong>Основная группа:</strong>
                                        <ul class="List-Inner">
                                            <li>Крик: Бельгийская брассерия,
                                                бельгийское пиво, ресторан
                                                бельгийской кухни;
                                            </li>
                                            <li>Ягерхаус: Немецкий ресторан,
                                                ресторан немецкого пива;
                                            </li>
                                            <li>Чешские пивовары: Ресторан
                                                чешской кухни.
                                            </li>
                                        </ul>

                                    </li>
                                    <li>
                                        <strong>Группа для подзадач:</strong>
                                        ресторан с верандой, пивной
                                        ресторан с футбольными
                                        трансляциями, ресторан с детской
                                        комнатой
                                    </li>
                                    <li>
                                        <strong>Группа географических ключевых слов</strong>
                                        ресторан в центре, ресторан на Невском проспекте
                                    </li>
                                    <li>
                                        <strong>Околотематические запросы:</strong>
                                        где выпить пива, посмотреть футбол в центре
                                    </li>
                                </ul>

                            </div>
                        </div>

                        <div class="Item JS-Carousel-Item Beer">
                            <h5>Тактика и цели</h5>


                            <div class="Cases-Item-Inner">


                                <ul class="Checklist Three">
                                    <li>
                                        <strong>Тактика работы:</strong>
                                        <ul class="List-Inner fsz-14">
                                            <li>Оптимизация сайта;</li>
                                            <li>Работа с текстами;</li>
                                            <li>Технические улучшения;</li>
                                            <li>Работа со ссылочной массой;</li>
                                            <li>Рекомендации по юзабилити и&nbsp;техническое внедрение.</li>
                                        </ul>
                                    </li>
                                    <li>
                                        <strong>Гипотеза:</strong>
                                        <ul class="List-Inner fsz-14">
                                            <li>10% тех, кто зайдет в раздел «Контакты», доедут до конкретного
                                                ресторана;
                                            </li>
                                            <li>Данные о среднем чеке предоставил клиент.</li>
                                        </ul>
                                    </li>
                                    <li class="">
                                        <strong>Цель:</strong>
                                        <span class="fsz-14">добиться конверсии в 2% из органического посетителя в посетителя ресторана</span>
                                    </li>
                                </ul>

                            </div>
                        </div>


                        <div class="Item JS-Carousel-Item Beer">
                            <h5>Метрики</h5>

                            <div class="tac fsz-18">

                                <ul class="lh-34 fw-500">
                                    <li>Трафик и его динамика</li>
                                    <li>Позиции и их динамика</li>
                                    <li>Количество заходов в контакты (промежуточная конверсия)</li>
                                    <li>Количество заходов в контакты конкретного ресторана</li>
                                    <li>Прогнозируемая конверсия в продажи</li>
                                    <li>Прогнозируемый ROI</li>
                                </ul>
                            </div>

                        </div>
                    </div>

                    <div class="Item JS-Carousel-Item Beer">
                        <h5>Результаты</h5>
                        <table class="Results-Table">
                            <tr>
                                <th>Ресторан 1</th>
                                <th>Поиск</th>
                                <th>Конверсия</th>
                                <th>Счёт</th>
                                <th>Доход, ₽</th>
                                <th>Цена конверсии, ₽</th>
                                <th>ROI SEO, ₽</th>
                            </tr>
                            <tr>
                                <th>Август</th>
                                <td>3161</td>
                                <td>63,22</td>
                                <td>1500</td>
                                <td>94 830</td>
                                <td>284,72</td>
                                <td>76 830</td>
                            </tr>
                            <tr>
                                <th>Сентябрь</th>
                                <td>3078</td>
                                <td>61,56</td>
                                <td>1500</td>
                                <td>92 340</td>
                                <td>292,40</td>
                                <td>74 340</td>
                            </tr>
                            <tr>
                                <th>Октябрь</th>
                                <td>3700</td>
                                <td>74</td>
                                <td>1500</td>
                                <td>111 000</td>
                                <td>243,24</td>
                                <td>93 000</td>
                            </tr>
                            <tr>
                                <th>Ноябрь</th>
                                <td>4007</td>
                                <td>80,14</td>
                                <td>1500</td>
                                <td>120 210</td>
                                <td>224,61</td>
                                <td>102 210</td>
                            </tr>
                            <tr>
                                <th>Декабрь</th>
                                <td>4246</td>
                                <td>84,92</td>
                                <td>1500</td>
                                <td>127 380</td>
                                <td>211,96</td>
                                <td>109 380</td>
                            </tr>
                            <tr>
                                <th>Январь</th>
                                <td>4347</td>
                                <td>86,94</td>
                                <td>1500</td>
                                <td>130 410</td>
                                <td>207,04</td>
                                <td>112 410</td>
                            </tr>
                            <tr>
                                <th>Февраль</th>
                                <td>4348</td>
                                <td>86,96</td>
                                <td>1500</td>
                                <td>130 440</td>
                                <td>206,99</td>
                                <td>112 440</td>
                            </tr>
                            <tr>
                                <th>Март</th>
                                <td>3922</td>
                                <td>78,44</td>
                                <td>1500</td>
                                <td>117 660</td>
                                <td>229,47</td>
                                <td>99 660</td>
                            </tr>
                        </table>
                    </div>


                    <div class="Item JS-Carousel-Item Beer">
                        <h5>Результаты</h5>
                        <table class="Results-Table">
                            <tr>
                                <th>Ресторан 2</th>
                                <th>Поиск</th>
                                <th>Конверсия</th>
                                <th>Счёт</th>
                                <th>Доход, ₽</th>
                                <th>Цена конверсии, ₽</th>
                                <th>ROI SEO, ₽</th>
                            </tr>
                            <tr>
                                <th>Август</th>
                                <td>2743</td>
                                <td>54,86</td>
                                <td>1500</td>
                                <td>82 290</td>
                                <td>382,79</td>
                                <td>61 290</td>
                            </tr>
                            <tr>
                                <th>Сентябрь</th>
                                <td>2986</td>
                                <td>59,72</td>
                                <td>1500</td>
                                <td>89 580</td>
                                <td>351,64</td>
                                <td>68 580</td>
                            </tr>
                            <tr>
                                <th>Октябрь</th>
                                <td>4007</td>
                                <td>80,14</td>
                                <td>1500</td>
                                <td>120 210</td>
                                <td>262,04</td>
                                <td>99 210</td>
                            </tr>
                            <tr>
                                <th>Ноябрь</th>
                                <td>3743</td>
                                <td>74,86</td>
                                <td>1500</td>
                                <td>112 290</td>
                                <td>280,52</td>
                                <td>91 290</td>
                            </tr>
                            <tr>
                                <th>Декабрь</th>
                                <td>4435</td>
                                <td>88,7</td>
                                <td>1500</td>
                                <td>133 050</td>
                                <td>236,75</td>
                                <td>112 050</td>
                            </tr>
                            <tr>
                                <th>Январь</th>
                                <td>3554</td>
                                <td>71,08</td>
                                <td>1500</td>
                                <td>106 620</td>
                                <td>295,44</td>
                                <td>85 620</td>
                            </tr>
                            <tr>
                                <th>Февраль</th>
                                <td>3952</td>
                                <td>79,04</td>
                                <td>1500</td>
                                <td>118 560</td>
                                <td>265,69</td>
                                <td>97 560</td>
                            </tr>
                            <tr>
                                <th>Март</th>
                                <td>4055</td>
                                <td>81,1</td>
                                <td>1500</td>
                                <td>121 650</td>
                                <td>258,94</td>
                                <td>100 650</td>
                            </tr>
                        </table>
                    </div>

                    <div class="Item JS-Carousel-Item Beer">
                        <h5>Результаты</h5>
                        <table class="Results-Table">
                            <tr>
                                <th>Ресторан 3</th>
                                <th>Поиск</th>
                                <th>Конверсия</th>
                                <th>Счёт</th>
                                <th>Доход, ₽</th>
                                <th>Цена конверсии, ₽</th>
                                <th>ROI SEO, ₽</th>
                            </tr>
                            <tr>
                                <th>Август</th>
                                <td>263</td>
                                <td>5,26</td>
                                <td>1500</td>
                                <td>7 890</td>
                                <td>3 422,05</td>
                                <td>- 10 110</td>
                            </tr>
                            <tr>
                                <th>Сентябрь</th>
                                <td>285</td>
                                <td>5,7</td>
                                <td>1500</td>
                                <td>8 550</td>
                                <td>3 157,89</td>
                                <td>- 9 450</td>
                            </tr>
                            <tr>
                                <th>Октябрь</th>
                                <td>584</td>
                                <td>11,68</td>
                                <td>1500</td>
                                <td>17 520</td>
                                <td>1 541,10</td>
                                <td>- 480</td>
                            </tr>
                            <tr>
                                <th>Ноябрь</th>
                                <td>698</td>
                                <td>13,96</td>
                                <td>1500</td>
                                <td>20 940</td>
                                <td>1 289,40</td>
                                <td>2 940</td>
                            </tr>
                            <tr>
                                <th>Декабрь</th>
                                <td>750</td>
                                <td>15</td>
                                <td>1500</td>
                                <td>22 500</td>
                                <td>1 200</td>
                                <td>4 500</td>
                            </tr>
                            <tr>
                                <th>Январь</th>
                                <td>909</td>
                                <td>18,18</td>
                                <td>1500</td>
                                <td>27 270</td>
                                <td>990,10</td>
                                <td>9 270</td>
                            </tr>
                            <tr>
                                <th>Февраль</th>
                                <td>961</td>
                                <td>19,22</td>
                                <td>1500</td>
                                <td>28 830</td>
                                <td>936,52</td>
                                <td>10 830</td>
                            </tr>
                            <tr>
                                <th>Март</th>
                                <td>1234</td>
                                <td>24,68</td>
                                <td>1500</td>
                                <td>37 020</td>
                                <td>729,34</td>
                                <td>19 020</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </li>

        </ul>
    </div>
</section>