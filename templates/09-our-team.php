<section class="Our-Team" id="team">
    <header class="Section-Heading">
        <h3>Команда</h3>
    </header>
    <article class="Team-About">
        Над вашим проектом работает команда из пяти специалистов. У каждого &mdash; своя зона ответственности.
        Координирует все отделы менеджер проекта &mdash; ответственный за продвижение. Для удобства всё общение вы
        ведёте с менеджером.
    </article>
    <div class="Team-Graph">
        <div class="Inner">
            <a href="#" class="Person Centrate-H" id="Client">
                <svg class="Icon Face">
                    <use xlink:href="#guy-03"></use>
                </svg>
                <p>Клиент</p>
                <ul class="Pop-Up">
                    <li>Координирует работу отделов</li>
                    <li>Разрабатывает стратегию</li>
                    <li>Следит за достижением KPI</li>
                    <li>Предоставляет отчёты</li>
                    <li>Консультирует клиента на каждом этапе</li>
                </ul>
            </a>
            <a href="#" class="Person Centrate-H" id="Project-Manager">
                <svg class="Icon Face">
                    <use xlink:href="#guy-03"></use>
                </svg>
                <svg class="Icon Info">
                    <use xlink:href="#info"></use>
                </svg>
                <p>Менеджер проекта</p>
                <ul class="Pop-Up">
                    <li>Координирует работу отделов</li>
                    <li>Разрабатывает стратегию</li>
                    <li>Следит за достижением KPI</li>
                    <li>Предоставляет отчёты</li>
                    <li>Консультирует клиента на каждом этапе</li>
                </ul>
            </a>

            <a href="#" class="Person" id="Designer">
                <svg class="Icon Face">
                    <use xlink:href="#girl-01"></use>
                </svg>
                <svg class="Icon Info">
                    <use xlink:href="#info"></use>
                </svg>
                <p>Дизайнер</p>
                <ul class="Pop-Up">
                    <li>Координирует работу отделов</li>
                    <li>Разрабатывает стратегию</li>
                    <li>Следит за достижением KPI</li>
                    <li>Предоставляет отчёты</li>
                    <li>Консультирует клиента на каждом этапе</li>
                </ul>
            </a>

            <a href="#" class="Person" id="Translator">
                <svg class="Icon Face">
                    <use xlink:href="#guy-01"></use>
                </svg>
                <svg class="Icon Info">
                    <use xlink:href="#info"></use>
                </svg>
                <p>Переводчик</p>
                <ul class="Pop-Up">
                    <li>Координирует работу отделов</li>
                    <li>Разрабатывает стратегию</li>
                    <li>Следит за достижением KPI</li>
                    <li>Предоставляет отчёты</li>
                    <li>Консультирует клиента на каждом этапе</li>
                </ul>
            </a>

            <div class="Bottom-Line">
                <a href="#" class="Person" id="Optimizer">
                    <svg class="Icon Face">
                        <use xlink:href="#guy-01"></use>
                    </svg>
                    <svg class="Icon Info">
                        <use xlink:href="#info"></use>
                    </svg>
                    <p>Оптимизатор</p>
                    <ul class="Pop-Up">
                        <li>Координирует работу отделов</li>
                        <li>Разрабатывает стратегию</li>
                        <li>Следит за достижением KPI</li>
                        <li>Предоставляет отчёты</li>
                        <li>Консультирует клиента на каждом этапе</li>
                    </ul>
                </a>

                <a href="#" class="Person" id="Copyrighter">
                    <svg class="Icon Face">
                        <use xlink:href="#girl-01"></use>
                    </svg>
                    <svg class="Icon Info">
                        <use xlink:href="#info"></use>
                    </svg>
                    <p>Копирайтер</p>
                    <ul class="Pop-Up">
                        <li>Координирует работу отделов</li>
                        <li>Разрабатывает стратегию</li>
                        <li>Следит за достижением KPI</li>
                        <li>Предоставляет отчёты</li>
                        <li>Консультирует клиента на каждом этапе</li>
                    </ul>
                </a>
                <a href="#" class="Person" id="Programmer">
                    <svg class="Icon Face">
                        <use xlink:href="#guy-01"></use>
                    </svg>
                    <svg class="Icon Info">
                        <use xlink:href="#info"></use>
                    </svg>
                    <p>Оптимизатор</p>
                    <ul class="Pop-Up">
                        <li>Координирует работу отделов</li>
                        <li>Разрабатывает стратегию</li>
                        <li>Следит за достижением KPI</li>
                        <li>Предоставляет отчёты</li>
                        <li>Консультирует клиента на каждом этапе</li>
                    </ul>
                </a>

                <a href="#" class="Person" id="Analytic">
                    <svg class="Icon Face">
                        <use xlink:href="#girl-01"></use>
                    </svg>
                    <svg class="Icon Info">
                        <use xlink:href="#info"></use>
                    </svg>
                    <p>Аналитик</p>
                    <ul class="Pop-Up">
                        <li>Координирует работу отделов</li>
                        <li>Разрабатывает стратегию</li>
                        <li>Следит за достижением KPI</li>
                        <li>Предоставляет отчёты</li>
                        <li>Консультирует клиента на каждом этапе</li>
                    </ul>
                </a>
            </div>

            <svg id="Arrow-Tree">
                <use xlink:href="#arrow-tree"></use>
            </svg>
            <svg id="Arrow-Tree-2">
                <use xlink:href="#arrow-tree-2"></use>
            </svg>
            <svg id="Arrow-Two-Sides">
                <use xlink:href="#arrow-two-sides"></use>
            </svg>
        </div>
    </div>
</section>