<!-- SECTION: Доходы клиентов -->
<section class="Client-Reviews" id="reviews">
    <header class="Section-Heading">
        <h3>За 2015 год наши клиенты<br>
            заработали&nbsp;35&nbsp;000&nbsp;000&nbsp;<span class="Rub">₽</span></h3>
    </header>

    <figure class="Review-Item">
        <img src="/img/team-photo-04.png" alt="">
        <figcaption> <p>Республика Кирибати состоит из 32 низменных атоллов и 1 поднятого атолла Банаба, или Ошен. Общая площадь суши составляет 812,34 км². Расстояние от самого западного до самого восточного острова республики составляет около 4000 км.</p></figcaption>
    </figure>

    <div class="Slider JS-Carousel Logo-Carousel">
        <ul class="Slider-Content JS-Carousel-List">
            <li class="JS-Carousel-Item">
                <img src="/img/slider-photo-01.png" alt="">
            </li>
            <li class="JS-Carousel-Item">
                <img src="/img/slider-photo-02.png" alt="">
            </li>
            <li class="JS-Carousel-Item">
                <img src="/img/slider-photo-03.png" alt="">
            </li>
            <li class="JS-Carousel-Item">
                <img src="/img/slider-photo-04.png" alt="">
            </li>
            <li class="JS-Carousel-Item">
                <img src="/img/slider-photo-05.png" alt="">
            </li>
            <li class="JS-Carousel-Item">
                <img src="/img/slider-photo-06.png" alt="">
            </li>
            <li class="JS-Carousel-Item">
                <img src="/img/slider-photo-03.png" alt="">
            </li>
            <li class="JS-Carousel-Item">
                <img src="/img/slider-photo-04.png" alt="">
            </li>
        </ul>
    </div>

</section>