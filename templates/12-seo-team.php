<!-- SECTION: Сео-подразделение -->
<section class="Seo-Team" id="seo">
    <header class="Section-Heading">
        <h3>Сео&ndash;подразделение</h3>
    </header>

    <article class="Team-List">
        <figure>
            <img src="/img/team-photo-01.png" alt="">
            <figcaption>
                <p class="Name">Станислав Усатый</p>

                <p class="Position">Управляющий партнёр</p>
            </figcaption>
        </figure>

        <figure>
            <img src="/img/team-photo-02.png" alt="">
            <figcaption>
                <p class="Name">Станислав Усатый</p>

                <p class="Position">Управляющий партнёр</p>
            </figcaption>
        </figure>

        <figure>
            <img src="/img/team-photo-03.png" alt="">
            <figcaption>
                <p class="Name">Станислав Усатый</p>

                <p class="Position">Управляющий партнёр</p>
            </figcaption>
        </figure>

        <figure>
            <img src="/img/team-photo-04.png" alt="">
            <figcaption>
                <p class="Name">Станислав Усатый</p>

                <p class="Position">Управляющий партнёр</p>
            </figcaption>
        </figure>

        <figure>
            <img src="/img/team-photo-05.png" alt="">
            <figcaption>
                <p class="Name">Станислав Усатый</p>

                <p class="Position">Управляющий партнёр</p>
            </figcaption>
        </figure>
    </article>

</section>