<section class="Leave-Request-Wide" id="order">

    <div class="Steps">
        <h3>Как мы увеличим ваши продажи</h3>

        <div class="Slider">
            <ul class="Slider-Content">
                <li class="Active">
                    <span class="Page-Number">1</span>
                    <p>
                        Вы <a href="#" class="Order-Hightlight">оставляете заявку</a><br>
                        Если удобнее &mdash; сразу звоните:<br>
                        +7 (495) 930&ndash;15&ndash;15
                    </p>
                </li>
                <li>
                    <span class="Page-Number">2</span>
                    <p>
                        Мы связываемся с вами<br>и вникаем в задачу
                    </p>
                </li>
                <li>
                    <span class="Page-Number">3</span>
                    <p>
                        Изучаем рынок,<br>погружаемся в ваш бизнес,<br>затем строим воронку продаж
                    </p>
                </li>
                <li>
                    <span class="Page-Number">4</span>
                    <p>
                        Заключаем договор, <br>а после — разрабатываем <br>и утверждаем стратегию
                    </p>
                </li>
                <li>
                    <span class="Page-Number">5</span>
                    <p>
                        Начинаем работать. <br>Получаем первые результаты: <br>посещения и продажи
                    </p>
                </li>
                <li>
                    <span class="Page-Number">6</span>
                    <p>
                        Анализируем цифры и <br>оптимизируем рекламу.<br> Корректируем стратегию,<br>если это необходимо.
                    </p>
                </li>
            </ul>
            <a class="Slider-Arrow Prev" href="#">
                <svg>
                    <use xlink:href="#arrow-thin"></use>
                </svg>
            </a>
            <a class="Slider-Arrow Next" href="#">
                <svg>
                    <use xlink:href="#arrow-thin"></use>
                </svg>
            </a>
        </div>

        <ol class="Steps-Graph">
            <li class="Active"></li>
            <li></li>
            <li></li>
            <li class="Illustration">
                <span>подписание</span>
                <svg>
                    <use xlink:href="#easel"></use>
                </svg>
                <span>договора</span>
            </li>
            <li></li>
            <li></li>
        </ol>
    </div>

    <?php require('06-leave-request.php'); ?>

</section>