<footer class="Page-Footer" id="contacts">

    <header class="Section-Heading">
        <h2>Контакты</h2>
    </header>

    <div class="Maps">
        <div class="Map Active JS-Map-Descroll" id="map_moscow">
<!--            <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=jB1yEq0uaU7-xjOKgZo5p5GM0igceSFs&width=100%&height=330&lang=ru_UA&sourceType=constructor&scroll=true"></script>-->
        </div>
        <div class="Map JS-Map-Descroll">
            <script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=8wTPoQcXRTELFOmkZN_LhsVyuu0wxira&width=100%&height=370"></script>
        </div>
    </div>

    <ul class="Footer-Tabs">
        <li class="Tab Active">
            <h6>Москва</h6>

            <p>Крымская набережная, 10<br>
                +7 (495) 668-30-12<br>
            <svg><use xlink:href="#boat"></use></svg></p>
        </li>
        <li class="Switch">
            <input id="Switch" type="checkbox">
            <label for="Switch">
                <span></span>
            </label>
        </li>
        <li class="Tab">
            <h6>Санкт&nbsp;Петербург</h6>

            <p>Васильевский остров, 12-я линия, д. 15<br>
               +7 (812) 903-07-20</p>
        </li>
    </ul>

    <div class="Footer-Links">
        <p>
            <a href="https://www.facebook.com/5oclick">Сообщество в Facebook</a>
        </p>

        <p>
            Эл. почта: <a href="mailto:sales@5oclick.ru">sales@5oclick.ru</a>
        </p>
    </div>  
</footer>